/*
 * Copyright 2013-2018 The NATS Authors
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* jslint node: true */
/* global describe: false, before: false, after: false, it: false */
'use strict';

var NATS = require('../'),
    nsc = require('./support/nats_server_control'),
    should = require('should');

describe('Stateless', function() {

    var PORT = 14123;
    var server;

    // Start up our own nats-server
    before(function(done) {
        server = nsc.start_server(PORT, done);
    });

    // Shutdown our server
    after(function(done) {
        nsc.stop_server(server, done);
    });

    it('should do basic subscribe and unsubscribe', function(done) {
        var nc = NATS.connect(PORT);
        nc.on('connect', function() {
            var sid = nc.subscribe('foo', { sid: 'foo' });
            should.exist(sid);
            nc.unsubscribe(sid);
            nc.flush(function() {
                nc.close();
                done();
            });
        });
    });

    it('should fire an event for subscription', function(done) {
        var nc = NATS.connect(PORT);

        nc.on('message', function(payload) {
            should.exist(payload);
            payload.sid.should.equal('bar');
            nc.close();
            done();
        });

        nc.on('connect', function() {
            nc.subscribe('foo', { sid: 'bar' });
            nc.publish('foo');
        });
    });

    it('should include the correct message in the event', function(done) {
        var nc = NATS.connect(PORT);
        var data = 'Hello World';

        
        nc.on('message', function(payload) {
            should.exist(payload);
            payload.msg.should.equal(data);
            nc.close();
            done();
        });

        nc.on('connect', function() {
            nc.subscribe('foo', { sid: 'bar' });
            nc.publish('foo', data);
        });
    });

    it('should do single partial wildcard subscriptions correctly', function(done) {
        var nc = NATS.connect(PORT);
        var expected = 3;
        var received = 0;
        
        nc.on('message', function(payload) {
            if (payload.sid !== 'bar') {
                return;
            }
            received += 1;
            if (received === expected) {
                nc.close();
                done();
            }
        });

        nc.on('connect', function() {
            nc.subscribe('*', { sid: 'bar' });
            nc.publish('foo.baz'); // miss
            nc.publish('foo.baz.foo'); // miss
            nc.publish('foo');
            nc.publish('bar');
            nc.publish('foo.bar.3'); // miss
            nc.publish('baz');
        });
    });

    it('should do partial wildcard subscriptions correctly', function(done) {
        var nc = NATS.connect(PORT);
        var expected = 3;
        var received = 0;
        
        nc.on('message', function(payload) {
            if (payload.sid !== 'bar') {
                return;
            }
            received += 1;
            if (received === expected) {
                nc.close();
                done();
            }
        });

        nc.on('connect', function() {
            nc.subscribe('foo.bar.*', { sid: 'bar' });

            nc.publish('foo.baz'); // miss
            nc.publish('foo.baz.foo'); // miss
            nc.publish('foo.bar.1');
            nc.publish('foo.bar.2');
            nc.publish('bar');
            nc.publish('foo.bar.3');
        });
    });

    it('should do full wildcard subscriptions correctly', function(done) {
        var nc = NATS.connect(PORT);
        var expected = 5;
        var received = 0;
        
        nc.on('message', function(payload) {
            if (payload.sid !== 'bar') {
                return;
            }
            received += 1;
            if (received === expected) {
                nc.close();
                done();
            }
        });

        nc.on('connect', function() {
            nc.subscribe('foo.>', { sid: 'bar' });

            nc.publish('foo.baz');
            nc.publish('foo.baz.foo');
            nc.publish('foo.bar.1');
            nc.publish('foo.bar.2');
            nc.publish('bar'); // miss
            nc.publish('foo.bar.3');
        });
    });

    it('should pass exact subject to callback', function(done) {
        var nc = NATS.connect(PORT);
        var subject = 'foo.bar.baz';
        
        nc.on('message', function(payload) {
            if (payload.sid !== 'bar') {
                return;
            }
            should.exist(payload.subj);
            payload.subj.should.equal(subject);
            nc.close();
            done();
        });

        nc.on('connect', function() {
            nc.subscribe('*.*.*', { sid: 'bar' });
            nc.publish(subject);
        });
    });


    it('should not receive data after unsubscribe call', function(done) {
        var nc = NATS.connect(PORT);
        var received = 0;
        var expected = 1;
        var sid;
        
        nc.on('message', function(payload) {
            if (payload.sid !== 'bar') {
                return;
            }
            nc.unsubscribe(sid);
            received += 1;
        });

        nc.on('connect', function() {
            sid = nc.subscribe('foo', { sid: 'bar' });
            nc.publish('foo');
            setTimeout(function() {
                nc.publish('foo');
                nc.publish('foo', function() {
                    received.should.equal(expected);
                    nc.close();
                    done();
                });
            }, 100);
        });
    });

});
